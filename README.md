## Trabajo Practico de Academia Java - Turnos

### Configuración inicial del modulo padre

Crear un proyecto maven, tildar la opción *create a simple project(skip archetype selection)*

1. GroupId: com.softtek.academia
2. ArtifactId: turnos
3. Packaging: pom


### Modulo webapp
1. Sobre el proyecto padre crear un modulo maven y seleccionar el arquetipo web. [imagen](./readme-img/create_maven_1.png)

1. Cambiar el descriptor en el web.xml. [imagen](./readme-img/web.xml)

1. Agregar Tomcat 7. [imagen](./readme-img/tomcat-1.png)

1. Agregar la aplicación turnos al deploy de tomcat. [imagen](./readme-img/agregar-webapp.png)

1. Cambiar el facet, agregar runtime y cambiar la versión de java de proyecto. [imagen](./readme-img/facelet-1.png)
 
1. Remover la versión de jre 1.5 y agregar una nueva librería de jre, seleccionar la de java 8. [imagen](./readme-img/build_path-1.png)

1. Configurar maven para que use java 8.

        <project>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-compiler-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <source>1.8</source>
                            <target>1.8</target>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </project>

1. Revisar la estructura actual de proyecto.

        turnos
        ├── pom.xml
        ├── src
        │   └── site
        └── turnos-webapp
            ├── pom.xml
            ├── src
            │   ├── main
            │   │   ├── java
            │   │   │   └── com
            │   │   │       └── softtek
            │   │   │           └── academia
            │   │   │               └── turnos
            │   │   │                   └── webapp
            │   │   │                       └── action
            │   │   │                           └── HelloWorldAction.java
            │   │   ├── resources
            │   │   │   ├── struts-mapping
            │   │   │   │   └── helloworld.xml
            │   │   │   └── struts.xml
            │   │   └── webapp
            │   │       ├── pages
            │   │       │   └── index.jsp
            │   │       └── WEB-INF
            │   │           └── web.xml
            │   └── test
            │       └── java
            └── target

1. Agregar un welcome file al [xml
	
        <welcome-file-list>
            <welcome-file>index.jsp</welcome-file>
        </welcome-file-list> 

1. Levantar tomcat y abrir la url [localhost:8080/turnos-webapp](localhost:8080/turnos-webapp). Debería mostrar el contenido del welcome file.

#### Agregar Struts

1. Agregar struts al pom del proyecto web.

        <dependency>
            <groupId>org.apache.struts</groupId>
            <artifactId>struts2-core</artifactId>
            <version>2.5.14.1</version>
        </dependency>

1. Agregar el filtro de struts al archivo web.xml.

        <filter>
            <filter-name>struts2</filter-name>
            <filter-class>org.apache.struts2.dispatcher.filter.StrutsPrepareAndExecuteFilter</filter-class>
        </filter>

        <filter-mapping>
            <filter-name>struts2</filter-name>
            <url-pattern>/*</url-pattern>
        </filter-mapping>

1. Crear el archivo struts.xml Dentro de la carpeta ** */src/main/resources* ** con el siguiente contenido.

        <?xml version="1.0" encoding="UTF-8" ?>
        <!DOCTYPE struts PUBLIC
        "-//Apache Software Foundation//DTD Struts Configuration 2.0//EN"
        "http://struts.apache.org/dtds/struts-2.0.dtd">

        <struts>

            <constant name="struts.devMode" value="true" />
            <include file="./struts-mapping/helloworld.xml"></include>

        </struts>

1. Crear la carpeta struts-mapping dentro ***/src/main/resources*** y crear un archivo xml (HelloWorld.xml en este caso).

        <?xml version="1.0" encoding="UTF-8" ?>
        <!DOCTYPE struts PUBLIC
        "-//Apache Software Foundation//DTD Struts Configuration 2.0//EN"
        "http://struts.apache.org/dtds/struts-2.0.dtd">

        <struts>
            <package name="helloworld" extends="struts-default">
                <action name="hello"
                    class="com.softtek.academia.turnos.webapp.action.HelloWorldAction"
                    method="execute">
                    <result name="success">/pages/index.jsp</result>
                </action>
            </package>
        </struts>

1. Crear la carpeta pages dentro de ***src/main/webapp/*** y agregar un jsp (index.jsp en este caso).

        <%@ page language="java" contentType="text/html; charset=UTF-8"
            pageEncoding="UTF-8"%>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index 2</title>
        </head>
        <body>
            <h1>Hello World Struts 2</h1>
        </body>
        </html>

1. Crear la clase. `com.softtek.academia.turnos.webapp.action.HelloWorldAction`
        package com.softtek.academia.turnos.webapp.action;

        import com.opensymphony.xwork2.ActionSupport;

        public class HelloWorldAction extends ActionSupport {

            private static final long serialVersionUID = 391332968538584897L;
            
            public String execute() {
                return SUCCESS;
            }
        }
1. La estructura del proyecto deberia haber quedado de la sigueinte manera. [imagen](./readme-img/struts/struts3.png)

1. Probar la aplicación entrando a [http://localhost:8080/turnos/hello](http://localhost:8080/turnos/hello) deberia mostrar el contenido del index.jsp

#### Agregar Spring y vincularlo con Struts

1. Agregar al pom parent las dependencias comunes de spring.

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring.version}</version>
        </dependency>
    
1. Agregar al pom del modulo web la dependencia del plugin de spring para struts.

        <dependency>
            <groupId>org.apache.struts</groupId>
            <artifactId>struts2-spring-plugin</artifactId>
            <version>${struts.version}</version>
        </dependency>

        <properties>
		    <struts.version>2.5.14.1</struts.version>
	    </properties>

1. Crear el archivo *applicationContext* en la carpeta resources con el siguiente contenido.

        <?xml version="1.0" encoding="UTF-8"?>
        <beans xmlns="http://www.springframework.org/schema/beans"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

            <import resource="./spring/*.xml"/>
        </beans>

1. Crear la carpeta spring y dentro el archivo actions.xml.

        <?xml version="1.0" encoding="UTF-8"?>
            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
                
                <bean id="helloWorldAction" class="com.softtek.academia.turnos.webapp.action.HelloWorldAction" />

            </beans>

4. Configurar el listener de spring en el web.xml.

        <context-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:applicationContext.xml</param-value>
        </context-param>
        
        <listener>
            <listener-class id="framework-spring-context-listener">
                org.springframework.web.context.ContextLoaderListener
            </listener-class>
        </listener>

6. structura del proyecto.

        turnos
        ├── pom.xml
        ├── src
        │   └── site
        └── turnos-webapp
            ├── pom.xml
            ├── src
            │   ├── main
            │   │   ├── java
            │   │   │   └── com
            │   │   │       └── softtek
            │   │   │           └── academia
            │   │   │               └── turnos
            │   │   │                   └── webapp
            │   │   │                       └── action
            │   │   │                           └── HelloWorldAction.java
            │   │   ├── resources
            │   │   │   ├── applicationContext.xml
            │   │   │   ├── spring
            │   │   │   │   └── actions.xml
            │   │   │   ├── struts
            │   │   │   │   └── helloworld.xml
            │   │   │   └── struts.xml
            │   │   └── webapp
            │   │       ├── pages
            │   │       │   └── index.jsp
            │   │       └── WEB-INF
            │   │           └── web.xml
            │   └── test
            │       └── java
            └── target


### Agregar modulo Service

1. Crear un modulo web como igual que el modulo webapp.
1. Agregar las dependencias de cxf y spring-web al pom del nuevo modulo.

		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
			<version>3.2.1</version>
		</dependency>

		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
			<version>3.2.1</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${spring.version}</version>
		</dependency>

1. Modificar el web.xml para agregar spring y cxf.

        <web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
                http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
            version="2.5">
            
            <context-param>
                <param-name>contextConfigLocation</param-name>
                <param-value>classpath:applicationContext.xml</param-value>
            </context-param>
            
            <listener>
                <listener-class id="framework-spring-context-listener">
                    org.springframework.web.context.ContextLoaderListener
                </listener-class>
            </listener>
            
            <servlet>
                <servlet-name>CXFServlet</servlet-name>
                <servlet-class>
                    org.apache.cxf.transport.servlet.CXFServlet
                </servlet-class>
                
            </servlet>
            
            <servlet-mapping>
                <servlet-name>CXFServlet</servlet-name>
                <url-pattern>/*</url-pattern>
            </servlet-mapping>
        </web-app>

1. Crear un archivo root de spring en la carpeta resources

        <?xml version="1.0" encoding="UTF-8"?>
        <beans xmlns="http://www.springframework.org/schema/beans"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:cxf="http://cxf.apache.org/core"
            xmlns:jaxws="http://cxf.apache.org/jaxws"
            xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                        http://cxf.apache.org/core http://cxf.apache.org/schemas/core.xsd
                        http://cxf.apache.org/jaxws http://cxf.apache.org/schemas/jaxws.xsd">
        <import resource="./spring/*.xml"/>

        </beans>

1. Crear un archivo webservices.xml en resources/spring

        <?xml version="1.0" encoding="UTF-8"?>
        <beans xmlns="http://www.springframework.org/schema/beans"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            xmlns:cxf="http://cxf.apache.org/core"
            xmlns:jaxws="http://cxf.apache.org/jaxws"
            xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                        http://cxf.apache.org/core http://cxf.apache.org/schemas/core.xsd
                        http://cxf.apache.org/jaxws http://cxf.apache.org/schemas/jaxws.xsd">

            <jaxws:endpoint id="helloWorld" address="/HelloWorld">
                    <jaxws:implementor>
                        <bean class="com.softtek.academia.turnos.service.HelloWorldImpl">
                        </bean>
                    </jaxws:implementor>
                </jaxws:endpoint>
        </beans>

1. Crear la clase HelloWorld y HelloworldImpl

        package com.softtek.academia.turnos.service;

        import javax.jws.WebMethod;
        import javax.jws.WebService;

        @WebService(name="HelloWorld")
        public interface HelloWorld {
            @WebMethod
            String sayHi(String text);
        }

        @WebService(endpointInterface = "com.softtek.academia.turnos.service.HelloWorld", serviceName="/HelloWorld")
        public class HelloWorldImpl implements HelloWorld {
            @Override
            public String sayHi(String text) {
                return "Hello world " + text;
            }
        }

7. Deployar el war de service y entrar a localhost:8080/turnos-service/ deberia listar los servicios.

### Configurar data source en tomcat y usarlo con spring

1. Agregar mysql-conector-java a la carpeta lib del server. 

1.  Modificar el archivo *context.xml* del Server y agregar lo siguiente modificando los valores de dbname, dbuser y dbpassword.

        <Resource auth="Container" driverClassName="com.mysql.jdbc.Driver"
            maxTotal="100" maxIdle="30" maxWaitMillis="10000" name="jdbc/academia"
            type="javax.sql.DataSource" url="jdbc:mysql://localhost:3306/dbname"
            username="dbuser" password="dbpassword" /> 

1. Modificar el archivo web.xml del proyecto y agregar una referencia a la Configuración del data source.

        <resource-ref >
            <description >MySQL Datasource example </description >
            <res-ref-name >jdbc/academia </res-ref-name >
            <res-type >javax.sql.DataSource </res-type >
            <res-auth >Container </res-auth >
        </resource-ref >

1. Agregar un bean que use el datasouce. 

        <bean id="Datasource" class="org.springframework.jndi.JndiObjectFactoryBean">
            <property name="jndiName" value="java:comp/env/jdbc/academia" />
        </bean>

1. Para testear crear un bean, inyectarle el data source e imprimir por consola los datos de conexión

         <bean id="repository" class="com.softtek.academia.turnos.service.common.entitie.PacienteRepository" >
            <constructor-arg name="dataSource" ref="Datasource"/>
         </bean>

        import java.sql.SQLException;

        import javax.sql.DataSource;

        public class PacienteRepository {
            private DataSource datasource;
            public PacienteRepository(DataSource dataSource) throws SQLException {
                datasource = dataSource;
                System.out.println(dataSource.getConnection());
            }
        }

### Agregar Hibernate y session factory.

1. Agregar las depencias de hibernate, spring y mysql al pom de service.

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.45</version>
        </dependency>

        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>4.1.4.Final</version>
        </dependency>

1. Crear un archivo hibernate.xml dentro de la carpeta spring con el datasource y la definicion del session factory.

        <?xml version="1.0" encoding="UTF-8"?>
            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cxf="http://cxf.apache.org/core"
                xmlns:jaxws="http://cxf.apache.org/jaxws"
                xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                            http://cxf.apache.org/core http://cxf.apache.org/schemas/core.xsd
                            http://cxf.apache.org/jaxws http://cxf.apache.org/schemas/jaxws.xsd">

                <bean id="Datasource" class="org.springframework.jndi.JndiObjectFactoryBean">
                    <property name="jndiName" value="java:comp/env/jdbc/academia" />
                </bean>

                <bean id="hibernate.sessionFactory"
                    class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
                    <property name="dataSource" ref="Datasource" />
                    <property name="annotatedClasses">
                        <list>
                            <value>com.softtek.academia.turnos.service.entities.Paciente
                            </value>
                        </list>
                    </property>
                </bean>
            </beans>

1. La clase paciente deberia ser una entidad que mapea con la base de datos. Un ejemplo posible seria:

        package com.softtek.academia.turnos.service.entities;

        import javax.persistence.Entity;
        import javax.persistence.Id;

        @Entity
        public class Paciente {
            @Id
            private int id;

            private String nombre;
            private String apellido;

            public Paciente() {}

            public Paciente(String nombre, String apellido) {
                this.nombre = nombre;
                this.apellido = apellido;
            }

            public void setId(int id) {this.id = id;}

            public String getNombre() {return nombre;}

            public void setNombre(String nombre) {this.nombre = nombre;}

            public String getApellido() {return apellido;}

            public void setApellido(String apellido) {this.apellido = apellido;}

            @Override
            public String toString() {
                return "Paciente [nombre=" + nombre + ", apellido=" + apellido + "]";
            }
        }

1. Definir y cear un objeto para acceder a la base de datos.
    1. definir el bean

            <?xml version="1.0" encoding="UTF-8"?>

            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

                <bean id="PacienteDao"
                    class="com.softtek.academia.turnos.service.dao.PacienteDao">
                    <property name="sessionFactory" ref="hibernate.sessionFactory"></property>
                </bean>
            </beans>
    2. Crear el objeto.

                package com.softtek.academia.turnos.service.dao;
                import java.util.List;
                import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
                import com.softtek.academia.turnos.service.entities.Paciente;
                public class PacienteDao extends HibernateDaoSupport {
                    
                    public List<Paciente> find() {
                        return (List<Paciente>) getHibernateTemplate().find("from Paciente");
                    }
                }

### Crear un cliente del servicio.

1. Agregar las dependencias de cxf en el front end.

1. Crear un xml para los clientes de web service los servicios

    1. xml

            <?xml version="1.0" encoding="UTF-8"?>
                    <beans xmlns="http://www.springframework.org/schema/beans"
                        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cxf="http://cxf.apache.org/core"
                        xmlns:jaxws="http://cxf.apache.org/jaxws"
                        xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                                    http://cxf.apache.org/core http://cxf.apache.org/schemas/core.xsd
                                    http://cxf.apache.org/jaxws http://cxf.apache.org/schemas/jaxws.xsd">

                        <jaxws:client id="serviceClient"
                            serviceClass="com.softtek.academia.turnos.webapp.service.client.paciente.PacienteService"
                            address="http://localhost:8080/turnos-services/pacientes" />
                    </beans>

    2. java

            package com.softtek.academia.turnos.webapp.service.client.paciente;

            import java.util.List;

            import javax.jws.WebService;

            import com.softtek.academia.turnos.webapp.service.client.paciente.entities.Paciente;

            @WebService(targetNamespace = "http://service.turnos.academia.softtek.com/")
            public interface PacienteService {
                List<Paciente> pacientes();
            }
